---
title: Everything became clear when I started writing code
date: 2016-10-20 18:44:46
tags:
---


<blockquote>
  <p>“…everything became clear when I started writing code.”</p>
</blockquote>

<script type="math/tex; mode=display">f(x,y) = x y</script>
