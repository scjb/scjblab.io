---
title: Welcome
menu: >
  <ul class="list-unstyled">
    <li><a href=#services>Services</a></li>
    <li><a href=#resume>Résumé</a></li>
  </ul>
  <a href="/docs/scjb_resume_en.pdf" class="btn btn-default" role="button">Download<br>Résumé</a>
---



<h1 id="services">Services
<a href=#services></a>
</h1>

* **Agile Digitalization:** Problem Analysis, Solution Design, Business Case Development, Project Management, Feasibility Studies, Organization & Architecture Maturity Assessments.
* **SAP Consulting:** SAP Project Management, SAP Business Objects (Analytics), SAP Business Warehouse (SAP BW), SAP Business Planning and Consolidation (SAP BPC), SAP SEM-BCS and SAP Integrated Planning (BI-IP) solutions.
* **Programming:** Android App Development, ABAP, SQL, Java, Visual Basic, JavaScript, HTML, CSS.
* **Data Analysis:** sourcing data from relational databases (MS SQL) and OLAP modeling tools (SAP BW, MS Analysis).

<h1 id="resume">Résumé
<a href=#resume></a>
</h1>

<table>
  <tr style="align:left">
    <th>03/2017 - today</th>
    <th>Manager, Capgemini Consulting</th>
    <th>Frankfurt a.M., Germany</th>
  </tr>
   <tr>
    <td><img src="/images/Capgemini Invent.jpg" alt="Capgemini Consulting" style="width:100px;"/></td>
    <td colspan="2">
      <ul>
        <li>Lead for digital transformation projects, from design-thinking based co-creation of new processes for new digital journeys to actual delivery, based on agile principles
        <li>Lead on Analytics and Performance Management Projects, including data warehouse design, KPI modelling, planning modelling and delivery
        <li>Organizing effective alignment between digital initiatives and business ownership, along multi-speed models combining agile and traditional
      </ul>
    </td>
  </tr>
</table>
<table>
  <tr style="align:left">
    <th>06/2014 - 07/2016</th>
    <th>Assistant Vice President, Group Technology, Deutsche Bank</th>
    <th>Frankfurt a.M., Germany</th>
  </tr>
  <tr>
    <td><img src="/images/deutsche_bank.png" alt="Deutsche Bank" style="width:100px;"/></td>
    <td colspan="2">
      <ul>
        <li>Direct a team of business analysts, architecting and implementing solutions for financial business processes.
        <li>Processes covered are investment banking sub-ledgers and group consolidation systems.
        <li>Manage projects & ensure delivery of end-to-end benefits for financial solutions.
        <li>Subject matter expert for financial consolidation, investment banking sub-ledgers and reporting systems.
      </ul>
    </td>
  </tr>
</table>
<table>
  <tr style="align:left">
    <th>09/2011 – 02/2014</th>
    <th>Senior Consultant, Business Analytics & Performance Management, IBM</th>
    <th>Singapore</th>
  </tr>
  <tr>
    <td><img src="/images/IBM.png" alt="IBM" style="width:100px;"/></td>
    <td colspan="2">
      <ul>
        <li>Responsible for business analytics & performance management project proposals and pre-sales.
        <li>Managing projects, from blueprint through to implementation and go-live
        <li>Business Analytics / Enterprise Performance Management subject matter expert and team lead, covering SAP BW/BI (data warehousing), SAP BPC (consolidation & planning) SAP Analytics (BusinessObjects) Web Intelligence & Dashboards, SAP HANA and legacy Business Explorer tools
      </ul>
    </td>
  </tr>
</table>
<table>
  <tr style="align:left">
    <th>03/2011 – 06/2011</th>
    <th>Consultant, Enterprise Performance Management, Freelancer for SAP</th>
    <th>Singapore</th>
  </tr>
  <tr>
    <td><img src="/images/SAP.png" alt="SAP" style="width: 100px;"/></td>
    <td colspan="2">
      <ul>
        <li>Responsible for all technical topics related to planning for a multi-national German automotive supplier.
        <li>Managed and executed all aspects of post-go live change requests including requirement analysis, design, implementation, testing, documentation and hand over.
        <li>Ensured the customer completes the first few planning runs successfully and on time. Solved every technical problem after go-live at a multi-national German automotive supplier.
      </ul>
    </td>
  </tr>
</table>
<table>
  <tr style="align:left">
    <th>01/2008 – 02/2011</th>
    <th>Consultant, Enterprise Performance Management, SAP</th>
    <th>Walldorf, Germany</th>
  </tr>
  <tr>
    <td><img src="/images/SAP.png" alt="SAP" style="width: 100px;"/></td>
    <td colspan="2">
      <ul>
        <li>Responsible for designing & implementing planning and analytical solutions based on SAP Business Planning and Consolidation (BPC), Integrated Planning and Business Warehouse.
        <li>Improved SAP BPC by designing and programming add-ons for financial and planning functionality.
        <li>Expert and point of contact for knowledge and guidance on SAP BPC within SAP Germany.
        <li>Won competitive pre-sales engagements by presenting solution and building proof-of-concepts.
        <li>Analyzed customer requirements, mapping them to solutions & designing the final system.
        <li>De-escalated projects by improving communication with customers and tracking & solving issues.
        <li>Collaborated in strongly international teams of various sizes.
      </ul>
    </td>
  </tr>
</table>
